<?php

namespace App;

use App\Csv\ReaderInterface;
use App\Csv\WriterInterface;
use App\Exceptions\ErrorException;
use App\Logger\LoggerInterface;

abstract class AbstractOperationFactory implements OperationInterface
{
    /**
     * Operation title
     * @var string
     */
    protected $title = '';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ReaderInterface
     */
    private $csvReader;

    /**
     * @var WriterInterface
     */
    private $csvWriter;

    public function __construct(ReaderInterface $csvReader,
                                WriterInterface $csvWriter,
                                LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->csvReader = $csvReader;
        $this->csvWriter = $csvWriter;

        // prepare files before handle operations
        $this->prepareFiles();
    }

    /**
     * @param $operation
     * @param array $params
     * @return mixed
     * @throws ErrorException
     */
    public static function create($operation, array $params)
    {
        $className = __NAMESPACE__ . '\\Operations\\' . $operation;
        if (!class_exists($className)) {
            throw new ErrorException('Wrong action is selected');
        }

        return new $className(...$params);
    }

    /**
     * Handle all operations
     */
    public function handle(): void
    {
        $this->getLogger()->log("Started {$this->getTitle()} operation \r\n");
        $rows = [];
        foreach ($this->getCsvReader()->getRows() as $columns) {
            $columns = $this->prepareNumbers($columns);

            if ($this->validateRow($columns)) {
                $rows[] = array_merge($columns, [
                    $this->calc($columns)
                ]);
            } else {
                $message = "numbers " . $columns[0] . " and " . $columns[1] . " are wrong \r\n";
                $this->getLogger()->log($message);
            }
        }

        $this->getCsvWriter()->write($rows);

        $this->getLogger()->log("Finished {$this->getTitle()} operation \r\n");
    }

    /**
     * Calculate columns result
     * @param array $columns
     * @return int
     */
    protected abstract function calc(array $columns): int;

    /**
     * Convert string[] to integer[] after trim it
     * @param array $values
     * @return array
     */
    protected function prepareNumbers(array $values): array
    {
        foreach ($values as $key => $value) {
            $values[$key] = (int)trim($value);
        }

        return $values;
    }

    /**
     * Prepare files for writing purpose
     */
    protected function prepareFiles(): void
    {
        // remove log file if exists
        $this->getLogger()->remove();

        // remove csv file if exists
        $this->getCsvWriter()->remove();;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return ReaderInterface
     */
    public function getCsvReader(): ReaderInterface
    {
        return $this->csvReader;
    }

    /**
     * @return WriterInterface
     */
    public function getCsvWriter(): WriterInterface
    {
        return $this->csvWriter;
    }

    /**
     * Return operation title
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}