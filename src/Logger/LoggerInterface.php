<?php

namespace App\Logger;

interface LoggerInterface
{
    /**
     * @param string $message
     */
    public function log(string $message): void;
}