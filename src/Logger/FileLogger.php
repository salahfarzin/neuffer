<?php

namespace App\Logger;

use App\Exceptions\ExceptionInterface;
use App\Exceptions\FileException;

class FileLogger implements LoggerInterface
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $mode = 'a+';

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Write new line to log file
     */
    public function log(string $message): void
    {
        try {
            $file = fopen($this->filePath, $this->mode);
            fwrite($file, $message);
            fclose($file);
        } catch (ExceptionInterface $e) {
            throw new FileException('Log File cannot be open for writing ' . $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * Check file exists
     * @return bool
     */
    public function exists(): bool
    {
        return file_exists($this->filePath);
    }

    /**
     * Remove file if exists
     * @return bool
     */
    public function remove(): bool
    {
        return $this->exists() && unlink($this->filePath);
    }
}