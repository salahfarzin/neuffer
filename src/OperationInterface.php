<?php

namespace App;

interface OperationInterface
{
    /**
     * Return operation title
     */
    public function getTitle(): string;

    /**
     * Handle the operation
     */
    public function handle(): void;

    /**
     * Validate columns
     */
    public function validateRow(array $columns): bool;
}