<?php

namespace App\Csv;

use App\Exceptions\ExceptionInterface;
use App\Exceptions\FileException;

class Writer implements WriterInterface
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $separator = ';';

    /**
     * @var string
     */
    private $mode = 'a+';

    /**
     * @var int
     */
    private $bufferSize = 4096;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return int
     */
    public function getBufferSize(): int
    {
        return $this->bufferSize;
    }

    /**
     * @param int $bufferSize
     */
    public function setBufferSize(int $bufferSize): void
    {
        $this->bufferSize = $bufferSize;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     */
    public function setSeparator(string $separator): void
    {
        $this->separator = $separator;
    }

    /**
     * @param array $rows
     * @param string $lineBreak
     * @return void
     */
    public function write(array $rows, string $lineBreak = " \r\n"): void
    {
        try {
            $file = fopen($this->filePath, $this->mode);
            foreach ($rows as $columns) {
                fwrite($file, implode($this->separator, $columns) . $lineBreak);
            }

            fclose($file);
        } catch (ExceptionInterface $e) {
            throw new FileException('File cannot be open for writing' . $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * Check file exists
     * @return bool
     */
    public function exists(): bool
    {
        return file_exists($this->filePath);
    }

    /**
     * Remove file if exists
     * @return bool
     */
    public function remove(): bool
    {
        return $this->exists() && unlink($this->filePath);
    }
}