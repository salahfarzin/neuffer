<?php

namespace App\Csv;

interface WriterInterface
{
    /**
     * @param array $rows
     * @param string $lineBreak
     */
    public function write(array $rows, string $lineBreak): void;
}