<?php

namespace App\Csv;

use App\Exceptions\ExceptionInterface;
use App\Exceptions\FileException;
use Generator;

class Reader implements ReaderInterface
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $separator = ';';

    /**
     * @var int
     */
    private $bufferSize = 4096;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return int
     */
    public function getBufferSize(): int
    {
        return $this->bufferSize;
    }

    /**
     * @param int $bufferSize
     */
    public function setBufferSize(int $bufferSize): void
    {
        $this->bufferSize = $bufferSize;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     */
    public function setSeparator(string $separator): void
    {
        $this->separator = $separator;
    }

    /**
     * @return Generator|null
     */
    public function getRows(): ?Generator
    {
        try {
            $file = fopen($this->filePath, 'r');
            while ($line = fgetcsv($file, $this->bufferSize, $this->separator)) {
                yield $line;
            }
            fclose($file);
        } catch (ExceptionInterface $e) {
            throw new FileException(' File cannot be open for reading ' . $e->getMessage(), $e->getCode());
        }

        return null;
    }
}