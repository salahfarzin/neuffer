<?php

namespace App\Csv;

interface ReaderInterface
{
    /**
     * @return mixed
     */
    public function getRows();
}