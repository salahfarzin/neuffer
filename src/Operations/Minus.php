<?php

namespace App\Operations;

use App\AbstractOperationFactory;

class Minus extends AbstractOperationFactory
{
    protected $title = 'minus';

    /**
     * Validate the columns value
     * @param array $columns
     * @return bool
     */
    public function validateRow(array $columns): bool
    {
        if ($this->calc($columns) < 0) return false;

        return true;
    }

    /**
     * Calculate columns result
     * @param array $columns
     * @return int
     */
    protected function calc(array $columns): int
    {
        return $columns[0] - $columns[1];
    }
}