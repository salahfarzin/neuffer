<?php

namespace App\Operations;

use App\AbstractOperationFactory;

class Plus extends AbstractOperationFactory
{
    protected $title = 'sum';

    /**
     * Validate the columns value
     * @param array $columns
     * @return bool
     */
    public function validateRow(array $columns): bool
    {
        $a = $columns[0];
        $b = $columns[1];

        if ($a < 0 && $b < 0 || ($a < 0 && abs($a) > $b) || ($b < 0 && abs($b) > $a)) {
            return false;
        }

        return true;
    }

    /**
     * @param array $columns
     * @return int
     */
    protected function calc(array $columns): int
    {
        return array_sum($columns);
    }
}