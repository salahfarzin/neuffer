<?php

namespace App\Operations;

use App\AbstractOperationFactory;

class Multiply extends AbstractOperationFactory
{
    protected $title = 'multiply';

    /**
     * Validate the columns value
     * @param array $columns
     * @return bool
     */
    public function validateRow(array $columns): bool
    {
        if ($this->calc($columns)) {
            return false;
        }

        return true;
    }

    /**
     * Calculate columns result
     * @param $columns
     * @return int
     */
    protected function calc($columns): int
    {
        return $columns[0] * $columns[1];
    }
}