<?php

namespace App\Operations;

use App\AbstractOperationFactory;

class Div extends AbstractOperationFactory
{
    protected $title = 'division';

    /**
     * Validate the columns value
     * @param array $columns
     * @return bool
     */
    public function validateRow(array $columns): bool
    {
        if ($columns[1] === 0) {
            return false;
        }

        return true;
    }

    /**
     * @param array $columns
     * @return int
     */
    protected function calc(array $columns): int
    {
        return $columns[0] / $columns[1];
    }
}