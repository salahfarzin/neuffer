<?php

use App\Csv\Reader;
use App\Csv\Writer;
use App\AbstractOperationFactory;
use App\Exceptions\ExceptionInterface;
use App\Exceptions\ErrorException;
use App\Logger\FileLogger;

/**
 * Register The Auto Loader
 */
require_once __DIR__ . '/vendor/autoload.php';

$options = getopt('a:f:', [
    'action:',
    'file:',
]);

$action = $options['a'] ?? $options['action'] ?? 'xyz';

$csvFile = $options['f'] ?? $options['file'] ?? 'notexists.csv';

try {
    AbstractOperationFactory::create(ucfirst(strtolower($action)), [
        new Reader($csvFile),
        new Writer('result.csv'),
        new FileLogger('test.log'),
    ])->handle();
} catch (ExceptionInterface $e) {
    throw new ErrorException($e->getMessage(), $e->getCode());
}
