# Neuffer 

Please before run install composer:

`composer install`

It is a small php-script, which should be started in console like:

`php console.php --action {action}  --file {file}`

For test with phpunit please use the following command: 

`./vendor/bin/phpunit`

For test one by one:

`./vendor/bin/phpunit tests/OperationsTest.php --filter=testPlus`

`./vendor/bin/phpunit tests/OperationsTest.php --filter=testMinus`

`./vendor/bin/phpunit tests/OperationsTest.php --filter=testMultiply`

`./vendor/bin/phpunit tests/OperationsTest.php --filter=testDiv`