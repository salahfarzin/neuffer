<?php declare(strict_types=1);

namespace App\Tests;

use App\Exceptions\ExceptionInterface;
use App\Operations\Div;
use App\Operations\Minus;
use App\Operations\Multiply;
use App\Operations\Plus;

class OperationsTest extends AbstractTestCase
{
    public function testPlus()
    {
        try {
            $plus = new Plus(...$this->getServices());
            $plus->handle();

            $this->assertTrue(true);
        } catch (ExceptionInterface $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testMinus()
    {
        try {
            $plus = new Minus(...$this->getServices());
            $plus->handle();

            $this->assertTrue(true);
        } catch (ExceptionInterface $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testMultiply()
    {
        try {
            $plus = new Multiply(...$this->getServices());
            $plus->handle();

            $this->assertTrue(true);
        } catch (ExceptionInterface $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDiv()
    {
        try {
            $plus = new Div(...$this->getServices());
            $plus->handle();

            $this->assertTrue(true);
        } catch (ExceptionInterface $e) {
            $this->fail($e->getMessage());
        }
    }
}