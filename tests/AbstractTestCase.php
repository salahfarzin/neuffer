<?php declare(strict_types=1);

namespace App\Tests;

use App\Csv\Reader;
use App\Csv\Writer;
use App\Logger\FileLogger;
use PHPUnit\Framework\TestCase;

class AbstractTestCase extends TestCase
{
    const CSV_FILE = 'test.csv';
    const CSV_RESULT = 'result.csv';
    const LOGGER_FILE = 'test.log';

    /**
     * @var array
     */
    private $services;

    /**
     * @var string
     */
    private $basePath = __DIR__ . '/../';

    /**
     * @return array
     */
    public function getServices(): array
    {
        return $this->services;
    }

    protected function setUp(): void
    {
        $this->services = [
            new Reader($this->basePath . self::CSV_FILE),
            new Writer($this->basePath . self::CSV_RESULT),
            new FileLogger($this->basePath . self::LOGGER_FILE)
        ];
    }
}